#include <iostream>
#include <string>
#include <queue>

#include "bdd_for_cpp.h"

static bool PASS = true;
static bool FAIL = false;

static const std::string COLOR_RESET = "\x1B[0m";
static const std::string COLOR_GREEN = "\x1B[32m";
static const std::string COLOR_RED = "\x1B[31m";
static const std::string FORMAT_BOLD = "\x1B[1m";

static const std::string PASS_SYMBOL = "\u2714";
static const std::string FAIL_SYMBOL = "\u2718";

static const std::string PASS_SYMBOL_GREEN_AND_BOLD = COLOR_GREEN + FORMAT_BOLD + PASS_SYMBOL + COLOR_RESET;
static const std::string FAIL_SYMBOL_RED_AND_BOLD = COLOR_RED + FORMAT_BOLD + FAIL_SYMBOL + COLOR_RESET;

static const std::string PASS_MESSAGE_FORMATTED = COLOR_GREEN + FORMAT_BOLD + "PASS" + COLOR_RESET;
static const std::string FAIL_MESSAGE_FORMATTED = COLOR_RED + FORMAT_BOLD + "FAIL" + COLOR_RESET;

std::queue<std::string*> reportableMessages;

void log(const std::string& message);
std::string formatReportMessage(const std::string& message, bool status);
void formatAndLogMessage(const std::string& message, bool status);
void executeTestScope(desc_t description, scope_t scopeFunction);

void describe(desc_t description, scope_t scopeFunction) {
    executeTestScope(description, scopeFunction);
}

void it(desc_t description, scope_t scopeFunction) {
    executeTestScope(description, scopeFunction);
}

void executeTestScope(desc_t description, scope_t scopeFunction) {
    try {
        scopeFunction();
        formatAndLogMessage(description, PASS);
    } catch (int err) {
        formatAndLogMessage(description, FAIL);
    }
}

void assertThat(bool expression) {
    if (!expression) {
        throw -1;
    }
}

void formatAndLogMessage(const std::string& message, bool status) {
    std::string formattedMessage = formatReportMessage(message, status);
    log(formattedMessage);
}

void log(const std::string& message) {
    std::cout << message << std::endl;
};

std::string formatReportMessage(const std::string& message, bool status) {
    std::string statusIcon = status ? PASS_SYMBOL_GREEN_AND_BOLD : FAIL_SYMBOL_RED_AND_BOLD;
    std::string statusMessage = status ? PASS_MESSAGE_FORMATTED : FAIL_MESSAGE_FORMATTED;
    std::string separator = " ";

    std::string formattedMessage = statusIcon +
                                   separator +
                                   message +
                                   separator +
                                   statusMessage;

    return formattedMessage;
}