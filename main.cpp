#include <iostream>

#include "bdd_for_cpp.h"

int main() {
    describe("Outer describe 1", [] {
        it("Inner it 1-1", [] {
            int64_t two = 2;
            int64_t three = 3;
            assertThat(two * three == 6);
        });

        it("Inner it 1-2", [] {
            int64_t two = 2;
            int64_t three = 3;
            assertThat(two * three == 23);
        });
    });

    describe("Outer describe 2", [] {
        describe("Inner describe 2-1", [] {
            it("Inner it 2-1-1", [] {
                int64_t two = 2;
                int64_t three = 3;
                assertThat(two * three == 6);
            });

            it("Inner it 2-1-2", [] {
                int64_t two = 2;
                int64_t three = 3;
                assertThat(two * three == 23);
            });
        });
    });
};