# bdd-for-cpp

**WORK IN PROGRESS**. Experimental bdd-for-c inspired minimalistic test library for C++11 and beyond.

Inspired by [mocha.js](http://mochajs.org).

## Desired usage

```c++
int main() {
    describe("bdd-for-cpp", [] {
        describe("method", [] {
            it("should return -1", [] {
                assertThat(method() == -1);
            });
        });
    }
}
```

## Desired output

```text
bdd-for-cpp
    method
        ✔ should return -1 PASS
		
0 test(s) failed. 1 test(s) passed. 1 test(s) total.
```

```text
bdd-for-cpp
    method
        ✘ this test fails FAIL
        ✔ this test passes PASS

1 test(s) failed. 1 test(s) passed. 2 test(s) total.
```

## Issues

- There are some issues with getting the desired output:
  - Output is currently ordered based on the function call order - the innermost functions (`it`) are reported first, then the outer functions (`describe`, `spec`).
- Inside a scope, if there are multiple `describe`s or `it`s, only the first one is executed as the `throw` causes the inner function to return its flow of control to the outer function earlier than desired.
- Certainly not adhering to SOLID principles.
- Need to redesign how the tests are actually executed.
    - To maintain the order of the tests.
