#ifndef BDD_FOR_CPP_LIBRARY_H
#define BDD_FOR_CPP_LIBRARY_H

// Minimalistic C++ testing support.
// Inspired by bdd-for-c, mocha.js, better-assert.
// Supports C++11 and above, as it relies on lambda functions and the <functional> library.
// Currently only supports command-line output.

#include <string>
#include <functional>

using desc_t = const std::string&;
using scope_t = const std::function<void(void)>&;

// Mocha.js-style `describe` and `it`.
// https://mochajs.org/
void describe(desc_t description, scope_t scopeFunction);
void it(desc_t description, scope_t scopeFunction);

// Minimal assertion support.
void assertThat(bool expression);

#endif